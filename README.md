# Arpachain Open Source Repo Readme

## Introduction

Welcome to the Arpachain open source repositories. Here, we are building a secure computation solution for any blockchain networks as a layer 2 scheme. Multi-party computation (MPC) technique is leveraged to realize this approach thanks to its privacy-preserving and verifiable properties. The workload can then be heavy-lifted from blockchain to the offchain network, which makes lower transaction fee and complex computation task possible.

Our computation nodes use linear secret sharing scheme (LSSS) based MPC as building block, and integrate with functionalities like autonomous networking, task distribution, and node maintenance. Besides the Arpachain network, proxy smart contracts are employed as the bridge to link the blockchain and secure computation network. Additionally, they will be used as verifier to ensure that outsourced computation is completed correctly.

Alongside the implementation of Arpachain, The experimental researches are also put in this group. To keep us updated with the cutting-edge MPC frameworks and other privacy techniques, we are continuously focusing on MPC related implementations and testing real-world use cases on them.

## Contents of the Repos

All repos under the gitlab group arpachain.io can be categorized into three types, including

1. Components of Arpachain secure computation network
   * arpa_smart_contract: the interface that connects Arpachain computation network with Ethereum.
   * coordinator: centralized coordinator responsible for network setup.
   * mainnet-mpc_agent: A script agent that starts MPC and sends the output via WebSocket on a VM instance.
   * mpc: Arpachain computation node core
   * mpc-network: An early version of Arpachain computation node core
   * mpc_prototype: A proof-of-concept website of MPC network with SCALE MAMBA. https://github.com/KULeuven-COSIC/SCALE-MAMBA 

2. MPC sample tasks for mainnet and testnet
   * example_mpc_programs: The MPC computation tasks under development.
   * mainnet-mpc-task: The sample MPC computation tasks run in mainnet.
   * whitepaper: The Latex source code of Arpachain whitepaper in English and Simplified Chinese.

3. Experiments on third-party MPC framework
   * private-join-and-compute: https://github.com/google/private-join-and-compute This is the MPC framework that implements high-performance private intersection. We modified it to apply to our specific use cases.
   * aby3: https://github.com/ladnir/aby3 This is the MPC framework that designs for privacy-preserving machine learning. We tried to use this on large-scale data analysis and cross tabulation.


## Warning

Despite Arpachain mainnet has been running for over a year and finished thousands of MPC computations. It still should **NOT** be considered fully secure. Only use this codebase as a proof-of-concept or benchmark. Future improvements and fixes are required to make it more sound, secure, and robust.